import os


txt_path = './test/working/'
filenames = []
for file in os.listdir(txt_path):
    if file[-6:] == 'u8.txt':
        filenames.append(file)

words_num = []
for file in filenames:
    with open(os.path.join(txt_path, file), 'rb') as f:
        for row in f.readlines():
            words_num.append(str(row).count('\\x'))
print(sum(words_num), sum(words_num) / 3)